package ru.titus.storage.postgres;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.sql.Timestamp;

@Entity
public class UnderstatSavedId extends AuditModel {

    @EmbeddedId
    private MongoEntityId mongoEntityId;

    public MongoEntityId getMongoEntityId() {
        return mongoEntityId;
    }

    public void setMongoEntityId(MongoEntityId mongoEntityId) {
        this.mongoEntityId = mongoEntityId;
    }

}
