package ru.titus.storage.postgres.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.storage.mongo.dto.League;
import ru.titus.storage.postgres.MongoEntityId;
import ru.titus.storage.postgres.UnderstatSavedId;
import ru.titus.storage.postgres.UnderstatSavedIdRepository;

import java.util.Date;
import java.util.Optional;

@Service
public class SaveIdDAO {

    @Autowired
    UnderstatSavedIdRepository savedIdsRepo;

    private MongoEntityId getTeamId(String team, League league, Integer year){
        MongoEntityId mei = new MongoEntityId();
        mei.setCollectionName("Team");
        mei.setMongoId(league.getId()+team+year);
        return mei;
    }

    private MongoEntityId getMatchId(String match) {
        MongoEntityId mei = new MongoEntityId();
        mei.setCollectionName("Match");
        mei.setMongoId(match);
        return mei;
    }

    private MongoEntityId getPlayerId(String player) {
        MongoEntityId mei = new MongoEntityId();
        mei.setCollectionName("Player");
        mei.setMongoId(player);
        return mei;
    }

    private void saveEntity(MongoEntityId id){
        UnderstatSavedId usi = new UnderstatSavedId();
        usi.setMongoEntityId(id);
        usi.setCreated(new Date(System.currentTimeMillis()));
        usi.setUpdated(new Date(System.currentTimeMillis()));
        savedIdsRepo.save(usi);
    }

    public void saveTeamId(String team, League league, Integer year){
        saveEntity(getTeamId(team,league,year));
    }

    public void saveMatchId(String match){
        saveEntity(getMatchId(match));
    }

    public void savePlayerId(String player){
        saveEntity(getPlayerId(player));
    }

    public boolean isTeamNotExisted(String team, League league, Integer year){
        Optional<UnderstatSavedId> existed = savedIdsRepo.findById(getTeamId(team,league,year));
        return !existed.isPresent();
    }

    public boolean isMatchNotExisted(String match){
        Optional<UnderstatSavedId> existed = savedIdsRepo.findById(getMatchId(match));
        return !existed.isPresent();
    }

    public boolean isPlayerNotExisted(String player){
        Optional<UnderstatSavedId> existed = savedIdsRepo.findById(getPlayerId(player));
        return !existed.isPresent();
    }

}
