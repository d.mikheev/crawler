package ru.titus.storage.postgres;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MongoEntityId implements Serializable {

    public String mongoId;
    public String collectionName;

    public String getMongoId() {
        return mongoId;
    }

    public void setMongoId(String mongoId) {
        this.mongoId = mongoId;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MongoEntityId that = (MongoEntityId) o;
        return mongoId.equals(that.mongoId) &&
                collectionName.equals(that.collectionName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mongoId, collectionName);
    }
}
