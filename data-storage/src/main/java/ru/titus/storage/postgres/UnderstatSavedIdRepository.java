package ru.titus.storage.postgres;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.titus.storage.mongo.dto.League;

import java.util.Optional;

@Repository
public interface UnderstatSavedIdRepository extends JpaRepository<UnderstatSavedId, MongoEntityId> {

}
