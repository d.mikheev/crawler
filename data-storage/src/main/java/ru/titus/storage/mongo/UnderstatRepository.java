package ru.titus.storage.mongo;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.storage.mongo.dto.League;
import ru.titus.storage.mongo.dto.UnderstatSave;

import java.util.*;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static java.util.stream.Collectors.toList;

@Service
public class UnderstatRepository {

    private static final Logger log = LoggerFactory.getLogger( UnderstatRepository.class);

    @Autowired
    private MongoDatabase mongo;

    public void createCollection(String collectionName){
        mongo.createCollection(collectionName);
    }

    /**
     * For saving understat entities
     * @param entity
     */
    public void saveDocument(UnderstatSave entity){
        if (entity.getData()==null)
            return;
        MongoCollection collection = mongo.getCollection(entity.getCollectionName());
        try {
            Document doc = Document.parse(entity.getData());
            entity.getParams().forEach((k,v)->doc.append(k,v));
            collection.replaceOne(eq(entity.getDedublicationElementName(), doc.get(entity.getDedublicationElementName())), doc, new ReplaceOptions().upsert(true));

        }
        catch (Exception e){
            log.info("Could't save data to collection "+entity.getCollectionName() +" entity id "+entity.getQuery(), e);
        }
    }

    public void usefulScript(){
        MongoCollection collection = mongo.getCollection("MatchTimeline");
        FindIterable events = collection.find(and(eq("queryUrl","match_timeline/15109029"))).projection(fields(excludeId(),include("doc.data.events")));//.projection(fields(include("doc")));
        events.into(new ArrayList());
    }

    public List<String> getTeamsNamesFromLeague(League league, int year) {
        MongoCollection collection = mongo.getCollection("U_LeagueTeamsParamsHistory");
        Document doc = (Document)collection.find(
                and(eq("league",String.valueOf(league.getId())),eq("year",String.valueOf(year)))).first();

        List<String> teamsKeys = doc.keySet().stream()
                .filter(key->key.matches("\\d\\d?\\d?\\d?"))
                .collect(toList());

        return teamsKeys.stream()
                .map(e->doc.get(e))
                .map(e->(Document)e)
                .map(e->(String)e.get("title"))
                .collect(toList());
    }

    public List<String> getTeamMatchesIds(String teamName, League league) {
        Collection<Document> aggr = mongo.getCollection("U_MatchTitle").aggregate(Arrays.asList(
                match(eq("league",String.valueOf(league.getId()))),
                match(or(eq("h.title",teamName),eq("a.title",teamName))),
                project(include("id"))
        )).into(new ArrayList());
        return aggr.stream().map(e->(String)e.get("id"))
                .collect(toList());
    }

    public List<String> getMatchPlayersIds(String teamName, int year) {
        Collection<Document> aggr = mongo.getCollection("U_Players").aggregate(Arrays.asList(
                match(eq("team_title",teamName)),
                match(eq("year",String.valueOf(year))),
                project(include("id"))
        )).into(new ArrayList());
        return aggr.stream().map(e->(String)e.get("id"))
                .collect(toList());
    }
}
