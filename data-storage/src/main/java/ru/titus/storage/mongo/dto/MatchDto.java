package ru.titus.storage.mongo.dto;

import java.sql.Timestamp;

public class MatchDto {
    private Integer id;
    private Integer seasonId;
    private Integer matchId;
    private Integer home;
    private Integer away;
    private Timestamp matchDate;
    private Timestamp updated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public Integer getHome() {
        return home;
    }

    public void setHome(Integer home) {
        this.home = home;
    }

    public Integer getAway() {
        return away;
    }

    public void setAway(Integer away) {
        this.away = away;
    }

    public Timestamp getMatchDate() { return matchDate; }

    public void setMatchDate(Timestamp matchDate) { this.matchDate = matchDate; }

    public Timestamp getUpdated() { return updated; }

    public void setUpdated(Timestamp updated) { this.updated = updated; }
}