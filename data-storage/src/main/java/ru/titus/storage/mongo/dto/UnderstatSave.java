package ru.titus.storage.mongo.dto;

import java.util.Map;

public interface UnderstatSave {

    public String getQuery();
    public String getCollectionName();
    public String getData();
    public Map<String,String> getParams();
    public String getDedublicationElementName();
}
