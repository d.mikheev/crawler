package ru.titus.storage.mongo;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.replaceRoot;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;


@Service
public class RadarRepository {
    @Autowired
    private MongoDatabase mongo;

    private static final Logger log = LoggerFactory.getLogger(RadarRepository.class);

    /**
     * For saving radar entities
     * @param dublicateLink
     * @param collectionName
     * @param data
     */
    public void saveDocument(String dublicateLink, String collectionName, String data){
        MongoCollection collection = mongo.getCollection(collectionName);
        try {
            Document doc = Document.parse(data);
            collection.replaceOne(eq("queryUrl", dublicateLink), doc, new ReplaceOptions().upsert(true));//data.events
        }
        catch (Exception e){
            log.info("Could't save data to collection "+collectionName, e);
        } //Arrays.asList(out("doc.data.events"))
    }

    public void getEvents(int matchid){
        MongoCollection collection = mongo.getCollection("MatchTimeline");
        String dublicateLink = "match_timeline/"+matchid;
        Collection<Document> aggr = collection.aggregate(Arrays.asList(
                match(eq("queryUrl",dublicateLink)),
                unwind("$doc"),
                replaceRoot("$doc.data"),
                unwind("$events"),
                replaceRoot("$events"),
                match(and(exists("X"),eq("type","goal")))
        )).into(new ArrayList());

        String out="";
        for(Document doc:aggr){
            out+="<p>"+doc.toJson()+"</p>";
        }
    }

    public void getMatchSituations(int matchid){
        MongoCollection collection = mongo.getCollection("MatchSituation");
        String dublicateLink = "stats_match_situation/"+matchid;
        Collection<Document> aggr = collection.aggregate(Arrays.asList(
                match(eq("queryUrl",dublicateLink)),
                unwind("$doc"),
                replaceRoot("$doc.data"),
                unwind("$data"),
                replaceRoot("$data"),
                match(and(exists("time"),exists("home"),exists("away")))
        )).into(new ArrayList());
    }
}
