package ru.titus.storage.mongo.dto;

import java.sql.Timestamp;

public class PlayerDto {

    private Integer id;
    private Timestamp updated;
    private Integer matchid;
    private Integer teamid;
    private String teamName;
    private Integer posType;
    private String posName;
    private Integer posOrder;
    private String matchPos;
    private String playerName;
    private String pos;
    private String shirtNumber;

    public PlayerDto(Integer matchid){
        this.matchid = matchid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Integer getMatchid() {
        return matchid;
    }

    public void setMatchid(Integer matchid) {
        this.matchid = matchid;
    }

    public Integer getTeamid() {
        return teamid;
    }

    public void setTeamid(Integer teamid) {
        this.teamid = teamid;
    }

    public Integer getPosType() {
        return posType;
    }

    public void setPosType(Integer posType) {
        this.posType = posType;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public Integer getPosOrder() {
        return posOrder;
    }

    public void setPosOrder(Integer posOrder) {
        this.posOrder = posOrder;
    }

    public String getMatchPos() {
        return matchPos;
    }

    public void setMatchPos(String matchPos) {
        this.matchPos = matchPos;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(String shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

}