package ru.titus.storage.mongo.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum League {

    EPL("EPL",1),
    LA_LIGA("La_liga",2),
    BUNDESLIGA("Bundesliga",3),
    SERIE_A("Serie_A",4),
    LIGUE_1("Ligue_1",5),
    RFPL("RFPL",6);

    private final String name;
    private final int id;

    League(String name, int id){
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static List<League> getAllLeagues(){
      return new ArrayList() {{
            add(EPL);
            add(LA_LIGA);
            add(BUNDESLIGA);
            add(SERIE_A);
            add(LIGUE_1);
            add(RFPL);
        }};
    }

    public static List<Integer> getValidYears(){
       return Arrays.asList(2017, 2018);
    }

}
