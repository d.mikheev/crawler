package ru.titus.crawler.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.titus.crawler.service.RadarCrawler;

@RestController
public class RadarController {

    @Autowired
    RadarCrawler radarCrawler;

    @RequestMapping(value="/season/{seasonid}", method= RequestMethod.GET)
    public void saveSeason(@PathVariable int seasonid){
        radarCrawler.addDataFromSeasonToQueue(seasonid);
        radarCrawler.processQueue();
    }

    @RequestMapping(value="/match/{matchid}", method= RequestMethod.GET)
    public void saveMatch(@PathVariable int matchid){
        radarCrawler.addDataFromMatchToQueue(matchid);
        radarCrawler.processQueue();
    }
}
