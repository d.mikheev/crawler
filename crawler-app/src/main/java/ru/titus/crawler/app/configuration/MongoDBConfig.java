package ru.titus.crawler.app.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoDBConfig {

    @Bean
    public MongoDatabase createDB(){
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase mongo = mongoClient.getDatabase("crawler");
        return mongo;
    }

}
