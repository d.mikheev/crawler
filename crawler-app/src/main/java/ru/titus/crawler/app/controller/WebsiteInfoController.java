package ru.titus.crawler.app.controller;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.titus.crawler.service.yandex.WebSiteInfoFinder;

@Controller
public class WebsiteInfoController {

    @Autowired
    WebSiteInfoFinder webSiteInfoFinder;

    @RequestMapping(value="/search", method= RequestMethod.GET)
    @ResponseBody
    public String searchContactsPage(){
        String siteUrl = "https://sk-onix.ru/";
        WebDriver webDriver = new FirefoxDriver();
        String contacts = webSiteInfoFinder.getContactPageContent(webDriver,siteUrl);
        return contacts;
    }
}
