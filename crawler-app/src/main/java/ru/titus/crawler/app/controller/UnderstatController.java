package ru.titus.crawler.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.titus.crawler.service.UnderstatCrawler;
import ru.titus.storage.mongo.dto.League;
import ru.titus.storage.mongo.UnderstatRepository;

@RestController
public class UnderstatController {

    @Autowired
    UnderstatRepository understatRepository;
    @Autowired
    UnderstatCrawler understatCrawler;

    @RequestMapping(value="/all", method= RequestMethod.GET)
    public void collectAll(){
        for (League league:League.getAllLeagues())
            for (Integer year:League.getValidYears())
                understatCrawler.crawlLeagueStat(league,year);
    }

    @RequestMapping(value="/status", method= RequestMethod.GET)
    public String status(){
        return understatCrawler.showProgress();
    }

    @RequestMapping(value="/stop", method= RequestMethod.GET)
    public String stop(){
        understatCrawler.stopExecutor();
        return "Executor finishing...";
    }

}
