package ru.titus.crawler.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import ru.titus.crawler.service.cbr.CBRService;
import ru.titus.crawler.service.cbr.Currency;
import ru.titus.crawler.service.cbr.ValuteContainer;
import java.util.Date;

@Controller
public class CBRController {

    @Autowired
    private CBRService cbrService;

    @RequestMapping(value="/currency/{currency}", method= RequestMethod.GET)
    @ResponseBody
    public String printCurrentDayRates(@PathVariable String currency){
        if (!Currency.isValid(currency))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,currency);

        ValuteContainer valuteContainer = cbrService.getCurrencyRateByCurrencyAndRate(Currency.valueOf(currency),new Date());
        return valuteContainer.getValuteList().toString();
    }


}
