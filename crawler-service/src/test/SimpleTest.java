import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

public class SimpleTest {

    @Test
    public void firstTest(){
        int[] a = {4,1,3};
        System.out.println(solution(a));
    }

    public int solution(int[] A){
        HashSet<Integer> set = new HashSet();
        for(int i=0;i<A.length;i++)
            set.add(A[i]);
        for(int i=1;i<=A.length;i++){
            if (!set.contains(i))
                return i;
        }
        return A.length+1;
    }

}
