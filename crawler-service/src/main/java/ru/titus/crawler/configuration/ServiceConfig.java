package ru.titus.crawler.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.titus.crawler.service.cbr.CBRClient;
import ru.titus.crawler.service.radar.impl.SportRadarMatchClient;
import ru.titus.crawler.service.radar.impl.SportRadarPlayerClient;

@Configuration
public class ServiceConfig {

    public final String statsBaseUrl = "https://wlc.fn.sportradar.com/";
    public final String cbrBaseUrl = "http://www.cbr.ru/scripts/";

    @Bean
    public SportRadarMatchClient createSportRadarMatchClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(statsBaseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        return retrofit.create(SportRadarMatchClient.class);
    }

    @Bean
    public SportRadarPlayerClient createSportRadarPlayerClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(statsBaseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        return retrofit.create(SportRadarPlayerClient.class);
    }

    @Bean
    public CBRClient createCBRClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(cbrBaseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        return retrofit.create(CBRClient.class);
    }
}
