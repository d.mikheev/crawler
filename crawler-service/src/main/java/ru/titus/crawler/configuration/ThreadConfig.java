package ru.titus.crawler.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadConfig {

    private final static int ENTITY_THREAD_PULL_SIZE = 4;
    private final static int ENTITY_AWAIT_TERMINATION_SEC = 30;
    private final static int QUEUE_CAPACITY = 5000;

    @Bean
    @Qualifier("importRadar")
    public ThreadPoolTaskExecutor importRadarTreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setCorePoolSize(ENTITY_THREAD_PULL_SIZE);
        executor.setMaxPoolSize(ENTITY_THREAD_PULL_SIZE);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(ENTITY_AWAIT_TERMINATION_SEC);
        executor.setThreadNamePrefix("before_task_executor_thread");
        return executor;
    }

    @Bean
    @Qualifier("importSelenium")
    public ThreadPoolTaskExecutor importSeleniumTreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setCorePoolSize(ENTITY_THREAD_PULL_SIZE);
        executor.setMaxPoolSize(ENTITY_THREAD_PULL_SIZE);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(ENTITY_AWAIT_TERMINATION_SEC);
        executor.setThreadNamePrefix("before_task_executor_thread");
        return executor;
    }

}
