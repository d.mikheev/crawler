package ru.titus.crawler.utils.parse.radar;

import org.codehaus.jackson.JsonNode;
import ru.titus.storage.mongo.dto.MatchDto;

import java.util.Set;

public class JsonMatchParser {

    private final static JsonParserUtils utils=new JsonParserUtils();

    public static Set<MatchDto> parseSeasonMatch(String json){
        JsonNode data = utils.getDataNode( json );
        Set<MatchDto> seasonMatches =  utils.getSeasonMatches(data);
        return seasonMatches;
    }

}
