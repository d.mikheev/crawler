package ru.titus.crawler.utils.parse.radar;

import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.titus.storage.mongo.dto.PlayerDto;

import java.util.HashSet;
import java.util.Set;

public class JsonPlayerParser {

    private static final Logger log = LoggerFactory.getLogger( JsonMatchParser.class);
    private final static JsonParserUtils utils=new JsonParserUtils();

    public static Set<PlayerDto> parseMatchPlayers(String json){
        Set<PlayerDto> players = new HashSet<>();
        JsonNode data = utils.getDataNode(json);
        Set<PlayerDto> homeSide = utils.addMatchPlayersFromSide( data, "home");
        Set<PlayerDto> awaySide = utils.addMatchPlayersFromSide( data, "away");
        players.addAll(homeSide);
        players.addAll(awaySide);
        return players;
    }

}
