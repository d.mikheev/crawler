package ru.titus.crawler.utils;

import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.titus.crawler.utils.parse.radar.JsonParserUtils;

public class JsonValidation {

    private static final Logger log = LoggerFactory.getLogger( JsonValidation.class);
    private final static JsonParserUtils utils=new JsonParserUtils();
    private final static int MIN_VALID_PARAMS_QTY = 10;

    public static boolean checkJsonExceptionEvent(String json){
        String queryUrl = utils.getQueryUrl( json );
        JsonNode data = utils.getDataNode( json );
        String doc = data.path("_doc").asText();
        if (doc.toLowerCase().equals("exception")){
            String message = data.path("message").asText();
            log.info(queryUrl+" "+message);
            return false;
        }
        return true;
    }

    public static boolean checkJsonMatchDetails(String json){
        String queryUrl = utils.getQueryUrl( json );
        JsonNode data = utils.getDataNode( json );
        JsonNode type = data.path("types");
        JsonNode index = data.path("index");
        if (type.size()>MIN_VALID_PARAMS_QTY&&index.size()>MIN_VALID_PARAMS_QTY)
            return true;
        log.info(queryUrl+" not enought params ({})",type.size());
        return false;
    }

    public static boolean checkJsonMatchSquads(String json){
        String queryUrl = utils.getQueryUrl( json );
        JsonNode data = utils.getDataNode( json );
        int homePlayersQty = utils.startingLineupSideSize(data,"home");
        int awayPlayersQty = utils.startingLineupSideSize(data,"away");
        if (homePlayersQty==11 && awayPlayersQty==11)
           return true;
        log.info(queryUrl+" not enought players home={}, away={})",homePlayersQty,awayPlayersQty);
        return false;
    }

    private static boolean matchStatus(String json){
        String queryUrl = utils.getQueryUrl( json );
        JsonNode data = utils.getDataNode( json );
        JsonNode match = data.path("match");
        JsonNode status = match.path("status");
        JsonNode name = status.path("name");
        if (utils.deleteQuotes(name.toString()).equals("Ended")){
            return true;
        }
        log.info(queryUrl+" match status=" + name.toString() + " is not 'Ended'");
        return false;
    }

    public static boolean checkJsonMatchInfo(String json){
        return matchStatus(json);
    }

    public static boolean checkJsonMatchTimeline(String json){
        return matchStatus(json);
    }
}
