package ru.titus.crawler.utils.parse.radar;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.titus.storage.mongo.dto.MatchDto;
import ru.titus.storage.mongo.dto.PlayerDto;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public final class JsonParserUtils {

    private static final Logger log = LoggerFactory.getLogger(JsonParserUtils.class);

    public static String deleteQuotes(String vsQuotes) {
        if (vsQuotes.length() > 2)
            if (vsQuotes.contentEquals("null")) {
            } else
                vsQuotes = vsQuotes.substring(1, vsQuotes.length() - 1);
        return vsQuotes;
    }

    public static Integer getTeamUidFromMatch(JsonNode match, String teamSide) {
        Integer teamName = 0;
        JsonNode teams = match.path("teams");
        JsonNode side = teams.path(teamSide);
        teamName = side.path("uid").asInt();
        return teamName;
    }

    public static String getQueryUrl(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            return root.path("queryUrl").toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JsonNode getDataNode(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode docNode : docRoot) {
                return docNode.path("data");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Timestamp convertStringtoTimestamp(String lastevent) {
        if (lastevent.length()<6)
            return  new Timestamp(System.currentTimeMillis());
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            if (lastevent.length() == 8) {
                dateFormat = new SimpleDateFormat("dd/MM/yy");
            }
            Date parsedDate = dateFormat.parse(lastevent);
            return  new Timestamp(parsedDate.getTime());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public static Set<MatchDto> getSeasonMatches(JsonNode data) {
        Set<MatchDto> seasonMatches = new HashSet<>();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        int seasonid = data.path("_id").asInt();
        JsonNode matches = data.path("matches");
        for (JsonNode match : matches) {
            MatchDto seasonMatch = new MatchDto();
            seasonMatch.setSeasonId(seasonid);
            seasonMatch.setMatchId(match.path("_id").asInt());
            seasonMatch.setHome(getTeamUidFromMatch(match, "home"));
            seasonMatch.setAway(getTeamUidFromMatch(match, "away"));
            JsonNode time = match.path("time");
            Timestamp matchDate = convertStringtoTimestamp(deleteQuotes(time.path("date").toString()));
            seasonMatch.setMatchDate(matchDate);
            seasonMatch.setUpdated(timestamp);
            seasonMatches.add(seasonMatch);
        }

        return seasonMatches;
    }

    public static String getTeamNameFromMatch(JsonNode match, String teamSide) {
        String teamName = "";
        JsonNode teams = match.path("teams");
        JsonNode side = teams.path(teamSide);
        teamName = deleteQuotes(side.path("name").toString());
        return teamName;
    }

    public static Set<PlayerDto> addMatchPlayersFromSide(JsonNode data, String teamSide) {
        Set<PlayerDto> sidePlayers = new HashSet<>();
        JsonNode match = data.path("match");
        Integer matchid = match.path("_id").asInt();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        JsonNode side = data.path(teamSide);
        JsonNode startingLineUp = side.path("startinglineup");
        JsonNode playersHome = startingLineUp.path("players");
        for (JsonNode player : playersHome) {
            PlayerDto matchPlayer = new PlayerDto(matchid);
            matchPlayer.setTeamName(getTeamNameFromMatch(match, teamSide));
            matchPlayer.setTeamid(getTeamUidFromMatch(match, teamSide));
            matchPlayer.setUpdated(timestamp);
            matchPlayer.setPosOrder(player.path("order").asInt());
            matchPlayer.setPosType(player.path("_type").asInt());
            matchPlayer.setPosName(deleteQuotes(player.path("name").toString()));
            matchPlayer.setMatchPos(deleteQuotes(player.path("matchpos").toString()));
            matchPlayer.setId(player.path("playerid").asInt());
            matchPlayer.setPlayerName(deleteQuotes(player.path("playername").toString()));
            matchPlayer.setPos(deleteQuotes(player.path("pos").toString()));
            matchPlayer.setShirtNumber(deleteQuotes(player.path("shirtnumber").toString()));
            sidePlayers.add(matchPlayer);
        }
        return sidePlayers;
    }

    public static int startingLineupSideSize(JsonNode data, String teamSide){
        JsonNode side = data.path(teamSide);
        JsonNode startingLineUp = side.path("startinglineup");
        JsonNode players = startingLineUp.path("players");
        return players.size();
    }
}