package ru.titus.crawler.utils.parse;

import org.bson.BsonArray;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public final class UnderstatJsonParser {

    public String prepareJSONStringify(String param){
        String statement = "return JSON.stringify(" + param + ");";
        return statement;
    }

    public String getJSONTeamNames(String param){
        String statement = "return JSON.stringify("+param+"[\"name\"]);";
        return statement;
    }

    public List<String> parseJsonArrayToStrings(String json){
        List<String> strings = new ArrayList<>();
        BsonArray doc = BsonArray.parse(json);
        for(BsonValue bv:doc.getValues()){
            String string = bv.toString();
            strings.add(string);
        }
        return strings;
    }

    public String getElementValue(String json, String elementName){
        Document doc = Document.parse(json);
        return (String) doc.get(elementName);
    }

    public String placeJsonArrayInElement(String json, String elementName){
        Document doc = new Document();
        doc.append(elementName,json);
        return doc.toJson();
    }



}




