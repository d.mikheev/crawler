package ru.titus.crawler.entities.understat;

public class RostersData extends UnderstatImportEntity {

    public final static String var = "rostersData";
    private final static String collection = "U_MatchPlayersStats";

    public RostersData(String matchData, String matchId) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "match/" + matchId;
        this.dedublicationElementName = "id";
        this.putParam("id", matchId.toString());
    }
}