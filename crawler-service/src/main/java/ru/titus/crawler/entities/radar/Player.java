package ru.titus.crawler.entities.radar;

public class Player extends RadarImportEntity
{
    public Player(int entityId){
        super(entityId);
        this.queryUrl = "stats_player_facts/";
        this.collectionName = "Player";
    }
}
