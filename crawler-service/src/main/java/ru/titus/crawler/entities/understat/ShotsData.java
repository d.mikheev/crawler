package ru.titus.crawler.entities.understat;

public class ShotsData extends UnderstatImportEntity {

    public final static String var = "shotsData";
    private final static String collection = "U_MatchShotsData";

    public ShotsData(String matchData, String matchId) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "match/" + matchId;
        this.dedublicationElementName = "id";
        this.putParam("id", matchId.toString());
    }
}