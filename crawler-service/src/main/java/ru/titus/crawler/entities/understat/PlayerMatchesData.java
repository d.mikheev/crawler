package ru.titus.crawler.entities.understat;

public class PlayerMatchesData extends UnderstatImportEntity {

    public final static String var = "matchesData";
    private final static String collection = "U_PlayerMatchesData";

    public PlayerMatchesData(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "playerId";
        this.putParam("playerId", playerId.toString());
    }
}
