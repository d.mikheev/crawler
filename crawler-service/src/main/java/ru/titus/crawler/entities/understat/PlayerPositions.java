package ru.titus.crawler.entities.understat;

public class PlayerPositions extends UnderstatImportEntity {

    public final static String var = "positionsList";
    private final static String collection = "U_PlayerPositions";

    public PlayerPositions(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "id";
        this.putParam("id", playerId.toString());
    }
}