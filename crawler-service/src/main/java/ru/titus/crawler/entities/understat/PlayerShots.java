package ru.titus.crawler.entities.understat;

public class PlayerShots extends UnderstatImportEntity {

    public final static String var = "shotsData";
    private final static String collection = "U_PlayerShots";

    public PlayerShots(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "id";
    }
}