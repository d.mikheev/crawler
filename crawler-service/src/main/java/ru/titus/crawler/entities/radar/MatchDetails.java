package ru.titus.crawler.entities.radar;

public class MatchDetails extends RadarImportEntity {

    public MatchDetails(int entityId){
        super(entityId);
        this.queryUrl = "match_detailsextended/";
        this.collectionName = "MatchDetails";
    }

    @Override
    public boolean isValid() {
        if (super.isValid()){
            return this.validation.checkJsonMatchDetails(this.data);
        }
        return false;
    }
}
