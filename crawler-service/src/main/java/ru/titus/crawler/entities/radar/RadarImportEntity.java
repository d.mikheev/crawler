package ru.titus.crawler.entities.radar;

import ru.titus.crawler.entities.ImportEntity;

public abstract class RadarImportEntity extends ImportEntity{
    public RadarImportEntity(int entityId) {
        super(entityId);
    }

    @Override
    public boolean isValid() {
        return validation.checkJsonExceptionEvent(data);
    }
}
