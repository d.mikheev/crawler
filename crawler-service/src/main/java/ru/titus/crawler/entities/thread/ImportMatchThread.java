package ru.titus.crawler.entities.thread;

import org.openqa.selenium.WebDriver;
import ru.titus.crawler.service.UnderstatCrawler;
import ru.titus.crawler.service.understat.UnderstatImporter;

public class ImportMatchThread implements Runnable{

    private final WebDriver driver;
    private final UnderstatImporter threadImporter;
    private final UnderstatCrawler crawler;
    private final String matchId;

    public ImportMatchThread(WebDriver driver, UnderstatCrawler crawler, UnderstatImporter threadImporter, String matchid) {
        this.driver = driver;
        this.crawler = crawler;
        this.threadImporter = threadImporter;
        this.matchId = matchid;
    }

    @Override
    public void run() {
        threadImporter.importMatch(driver,matchId);
        crawler.addToQueue(driver);
    }
}
