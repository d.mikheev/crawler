package ru.titus.crawler.entities;

import ru.titus.crawler.utils.JsonValidation;

public abstract class ImportEntity {

    protected String queryUrl;
    protected String collectionName;
    protected String data;
    protected int id;

    protected final static JsonValidation validation = new JsonValidation();
    public ImportEntity(int entityId){
        this.id = entityId;
    }

    public ImportEntity(){
    }

    public boolean isValid(){
        return true;
    }

    public String getQueryUrl() {
        return queryUrl;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
