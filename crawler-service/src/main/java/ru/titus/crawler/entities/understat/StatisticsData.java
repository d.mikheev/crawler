package ru.titus.crawler.entities.understat;

import ru.titus.storage.mongo.dto.League;

public class StatisticsData extends UnderstatImportEntity{

    public final static String var = "statisticsData";
    private final static String collection = "U_TeamStatistic";

    public StatisticsData(String matchData, String team, Integer year, League league) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "team/"+team+"/"+year;
        this.dedublicationElementName = "primaryKey";
        this.putParam("team", team);
        this.putParam("year", year.toString());
        this.putParam("league",String.valueOf(league.getId()));
        this.putParam("primaryKey", team+year.toString());
    }
}
