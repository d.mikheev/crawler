package ru.titus.crawler.entities.thread;

import org.openqa.selenium.WebDriver;
import ru.titus.crawler.service.UnderstatCrawler;
import ru.titus.crawler.service.understat.UnderstatImporter;
import ru.titus.storage.mongo.dto.League;

public class ImportTeamThread implements Runnable {

    private final WebDriver driver;
    private final UnderstatImporter threadImporter;
    private final UnderstatCrawler crawler;
    private final League league;
    private final String team;
    private final Integer year;

    public ImportTeamThread(WebDriver driver, UnderstatCrawler crawler, UnderstatImporter threadImporter, League league, String team, Integer year) {
        this.driver = driver;
        this.threadImporter = threadImporter;
        this.crawler = crawler;
        this.league = league;
        this.year = year;
        this.team = team;
    }

    @Override
    public void run() {
        threadImporter.importTeamSeason(driver,team,league,year);
        crawler.addToQueue(driver);
    }
}
