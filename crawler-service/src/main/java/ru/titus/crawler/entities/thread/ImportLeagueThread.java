package ru.titus.crawler.entities.thread;

import org.openqa.selenium.WebDriver;
import ru.titus.crawler.service.understat.UnderstatImporter;
import ru.titus.storage.mongo.dto.League;

public class ImportLeagueThread implements Runnable{

    private final WebDriver driver;
    private final UnderstatImporter threadImporter;
    private final League league;
    private final Integer year;

    public ImportLeagueThread(WebDriver webDriver, UnderstatImporter threadImporter, League league, Integer year) {
        this.driver = webDriver;
        this.threadImporter = threadImporter;
        this.league = league;
        this.year = year;
    }

    @Override
    public void run() {
        threadImporter.importLeague(driver,league,year);
    }
}
