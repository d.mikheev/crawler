package ru.titus.crawler.entities.understat;

public class GroupsData extends UnderstatImportEntity {

    public final static String var = "groupsData";
    private final static String collection = "U_PlayerGroupsData";

    public GroupsData(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "playerId";
        this.putParam("playerId", playerId.toString());
    }
}