package ru.titus.crawler.entities.understat;

public class MinMaxPlayersStats extends UnderstatImportEntity {

    public final static String var = "minMaxPlayerStats";
    private final static String collection = "U_MinMaxPlayerStats";

    public MinMaxPlayersStats(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "id";
        this.putParam("id", playerId.toString());
    }
}