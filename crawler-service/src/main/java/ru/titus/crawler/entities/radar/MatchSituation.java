package ru.titus.crawler.entities.radar;

public class MatchSituation extends RadarImportEntity {

    public MatchSituation(int entityId){
        super(entityId);
        this.queryUrl = "stats_match_situation/";
        this.collectionName = "MatchSituation";
    }

}
