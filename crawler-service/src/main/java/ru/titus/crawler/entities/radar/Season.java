package ru.titus.crawler.entities.radar;

public class Season extends RadarImportEntity {

    public Season(int entityId) {
        super(entityId);
        this.queryUrl = "stats_season_fixtures2/";
        this.collectionName = "Season";
    }
}
