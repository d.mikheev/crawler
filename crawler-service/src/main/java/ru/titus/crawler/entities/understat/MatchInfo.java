package ru.titus.crawler.entities.understat;

public class MatchInfo extends UnderstatImportEntity {

    public final static String var = "match_info";
    private final static String collection = "U_MatchInfo";

    public MatchInfo(String matchData, String matchId) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "match/" + matchId;
        this.dedublicationElementName = "id";
    }
}
