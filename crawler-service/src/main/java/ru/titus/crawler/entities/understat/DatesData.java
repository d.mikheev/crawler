package ru.titus.crawler.entities.understat;

import ru.titus.storage.mongo.dto.League;

public class DatesData extends UnderstatImportEntity{

    public final static String var = "datesData";
    private final static String collection = "U_MatchTitle";

    public DatesData(String matchData, League league, Integer year) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "league/"+league+"/"+year;
        this.dedublicationElementName = "id";
        this.putParam("league", String.valueOf(league.getId()));
    }

    public DatesData(String matchData, Integer year, String teamName, League league) {
        this.data = matchData;
        this.collectionName = collection;
        this.queryUrl = "team/"+teamName+"/"+year;
        this.dedublicationElementName = "id";
        this.putParam("league", String.valueOf(league.getId()));
    }

}
