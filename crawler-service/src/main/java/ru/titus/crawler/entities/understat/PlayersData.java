package ru.titus.crawler.entities.understat;

import ru.titus.storage.mongo.dto.League;

public class PlayersData extends UnderstatImportEntity{

    public final static String var = "playersData";
    private final static String collection = "U_Players";

    public PlayersData(String playersData, String playerId, League league, Integer year) {
        this.data = playersData;
        this.collectionName = collection;
        this.queryUrl = "league/"+league+"/"+year;
        this.dedublicationElementName = "primaryKey";
        this.putParam("league", String.valueOf(league.getId()));
        this.putParam("year", year.toString());
        this.putParam("primaryKey", playerId + year.toString());
    }

    public PlayersData(String playersData, String playerId, Integer year, String team, League league) {
        this.data = playersData;
        this.collectionName = collection;
        this.queryUrl = "team/"+team+"/"+year;
        this.dedublicationElementName = "primaryKey";
        this.putParam("year", year.toString());
        this.putParam("primaryKey", playerId+year.toString());
        this.putParam("league", String.valueOf(league.getId()));
    }
}
