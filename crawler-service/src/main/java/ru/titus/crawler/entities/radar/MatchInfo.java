package ru.titus.crawler.entities.radar;

public class MatchInfo extends RadarImportEntity {

    public MatchInfo(int entityId){
        super(entityId);
        this.queryUrl = "match_info/";
        this.collectionName = "MatchInfo";
    }
}
