package ru.titus.crawler.entities.understat;

import ru.titus.storage.mongo.dto.League;

public class TeamsDate extends UnderstatImportEntity{

    private final static String subUrl = "league";
    public final static String var = "teamsData";
    private final static String collection = "U_LeagueTeamsParamsHistory";

    public TeamsDate(String teamsData, League league, Integer year) {
        this.data = teamsData;
        this.collectionName = collection;
        this.queryUrl = subUrl+"/"+league+"/"+year;
        this.dedublicationElementName = "queryUrl";
        this.putParam("league", String.valueOf(league.getId()));
        this.putParam("year", year.toString());
        this.putParam("queryUrl", queryUrl);
    }
}
