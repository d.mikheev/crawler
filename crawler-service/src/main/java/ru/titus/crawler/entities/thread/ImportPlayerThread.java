package ru.titus.crawler.entities.thread;

import org.openqa.selenium.WebDriver;
import ru.titus.crawler.service.UnderstatCrawler;
import ru.titus.crawler.service.understat.UnderstatImporter;

public class ImportPlayerThread implements Runnable {

    private final WebDriver driver;
    private final UnderstatImporter threadImporter;
    private final UnderstatCrawler crawler;
    private final String playerId;

    public ImportPlayerThread(WebDriver driver, UnderstatCrawler crawler, UnderstatImporter threadImporter, String playerId) {
        this.driver = driver;
        this.crawler = crawler;
        this.threadImporter = threadImporter;
        this.playerId = playerId;
    }

    @Override
    public void run() {
        threadImporter.importPlayer(driver,playerId);
        crawler.addToQueue(driver);
    }
}
