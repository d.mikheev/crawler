package ru.titus.crawler.entities.understat;

import ru.titus.crawler.entities.ImportEntity;
import ru.titus.storage.mongo.dto.UnderstatSave;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class UnderstatImportEntity extends ImportEntity implements UnderstatSave {
    public static String baseUrl = "https://understat.com/";
    private Map<String,String> params = new LinkedHashMap<>();
    protected String dedublicationElementName;

    public Map<String, String> getParams() {
        return params;
    }

    public String getQuery() { return queryUrl;}

    public void putParam(String key, String value){
        this.params.put(key,value);
    }

    public String getDedublicationElementName() {
        return dedublicationElementName;
    }
}
