package ru.titus.crawler.entities.radar;

public class MatchSquads extends RadarImportEntity {

    public MatchSquads(int entityId){
        super(entityId);
        this.queryUrl = "match_squads/";
        this.collectionName = "MatchSquads";
    }

    @Override
    public boolean isValid() {
        if (super.isValid()){
            return this.validation.checkJsonMatchSquads(this.data);
        }
        return false;
    }
}

