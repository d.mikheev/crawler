package ru.titus.crawler.entities.radar;

public class MatchTimeline extends RadarImportEntity {

    public MatchTimeline(int entityId){
        super(entityId);
        this.queryUrl = "match_timeline/";
        this.collectionName = "MatchTimeline";
    }
}
