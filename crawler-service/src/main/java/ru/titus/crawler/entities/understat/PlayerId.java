package ru.titus.crawler.entities.understat;

public class PlayerId extends UnderstatImportEntity {

    public final static String var = "player_info";
    private final static String collection = "U_PlayerId";

    public PlayerId(String playerData, String playerId) {
        this.data = playerData;
        this.collectionName = collection;
        this.queryUrl = "player/" + playerId;
        this.dedublicationElementName = "id";
    }
}