package ru.titus.crawler.service.cbr;


import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValuteContainer {
    @XmlAttribute(name = "Date")
    private String reqDate;
    @XmlAttribute(name = "name")
    private String name;
    @XmlElement(name="Valute")
    private List<Valute> valuteList;

    public ValuteContainer(){};

    public ValuteContainer(String reqDate, String name, List<Valute> valuteList) {
        this.reqDate = reqDate;
        this.name = name;
        this.valuteList = valuteList;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Valute> getValuteList() {
        return valuteList;
    }

    public void setValuteList(List<Valute> valuteList) {
        this.valuteList = valuteList;
    }
}
