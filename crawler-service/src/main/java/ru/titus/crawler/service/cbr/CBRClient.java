package ru.titus.crawler.service.cbr;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CBRClient {

    @GET("XML_daily.asp")
    Call<String> getDailyRates(@Query(value = "date_req", encoded=true) String dateStr);

}
