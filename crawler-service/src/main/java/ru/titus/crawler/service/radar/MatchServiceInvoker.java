package ru.titus.crawler.service.radar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.crawler.entities.ImportEntity;
import ru.titus.crawler.entities.radar.*;

@Service
public class MatchServiceInvoker {
    @Autowired MatchService matchService;
    @Autowired PlayerService playerService;

    public ImportEntity enrich(MatchSituation matchSituation){
        String data = matchService.getMatchSituation(matchSituation.getId());
        matchSituation.setData(data);
        return matchSituation;
    }

    public ImportEntity enrich(Season season){
        String data = matchService.getMatchSituation(season.getId());
        season.setData(data);
        return season;
    }

    public ImportEntity enrich(MatchInfo matchInfo){
        String data = matchService.getMatchInfo(matchInfo.getId());
        matchInfo.setData(data);
        return matchInfo;
    }

    public ImportEntity enrich(MatchDetails matchDetails){
        String data = matchService.getMatchDetails(matchDetails.getId());
        matchDetails.setData(data);
        return matchDetails;
    }

    public ImportEntity enrich(MatchTimeline matchTimeline){
        String data = matchService.getMatchTimeline(matchTimeline.getId());
        matchTimeline.setData(data);
        return matchTimeline;
    }

    public ImportEntity enrich(MatchSquads matchSquads){
        String data = playerService.getMatchSquads(matchSquads.getId());
        matchSquads.setData(data);
        return matchSquads;
    }

    public Player enrich(Player player){
        String data = playerService.getPlayerStats(player.getId());
        player.setData(data);
        return player;
    }

    public ImportEntity enrich(ImportEntity entity){
        if (entity instanceof MatchSituation)
            return enrich((MatchSituation)entity);
        if (entity instanceof Season)
            return enrich((Season)entity);
        if (entity instanceof MatchInfo)
            return enrich((MatchInfo)entity);
        if (entity instanceof MatchDetails)
            return enrich((MatchDetails)entity);
        if (entity instanceof MatchTimeline)
            return enrich((MatchTimeline)entity);
        if (entity instanceof MatchSquads)
            return enrich((MatchSquads)entity);
        if (entity instanceof Player)
            return enrich((Player)entity);


        throw new IllegalArgumentException("Unknown child subtype");
    }


}
