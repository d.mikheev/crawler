package ru.titus.crawler.service.radar.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.crawler.service.radar.PlayerService;

import java.io.IOException;

@Service
public class PlayerServiceImpl implements PlayerService {
    private static final Logger logger = LoggerFactory.getLogger(PlayerServiceImpl.class);
    private final SportRadarPlayerClient client;

    @Autowired
    public PlayerServiceImpl(SportRadarPlayerClient client) {
        this.client = client;
    }

    @Override
    public String getMatchSquads(Integer matchId) {
        try {
            return client.getMatchSquads(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getPlayerStats(Integer playerid) {
        try {
            return client.getPlayerStats(playerid).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }
}



