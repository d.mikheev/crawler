package ru.titus.crawler.service.radar;

import java.util.Date;

public interface MatchService {

    public String getMatchesTimetable(Date date);

    public String getMatchDetails(Integer matchId);

    public String getMatchInfo(Integer matchId);

    public String getMatchSituation(Integer matchId);

    public String getTeamLastMatches(Integer teamId);

    public String getTeamForm(Integer matchId);

    public String getTeamsHeadToHeadMatches(Integer homeid, Integer awayid);

    public String getMatchTimeline(Integer matchId);

    public String getSeasonPastMatches(Integer seasonId);

}
