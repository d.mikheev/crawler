package ru.titus.crawler.service;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import ru.titus.crawler.entities.thread.ImportMatchThread;
import ru.titus.crawler.entities.thread.ImportPlayerThread;
import ru.titus.storage.mongo.dto.League;
import ru.titus.crawler.service.understat.UnderstatImporter;
import ru.titus.storage.mongo.UnderstatRepository;
import ru.titus.storage.postgres.DAO.SaveIdDAO;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Predicate;

@Service
public class UnderstatCrawler {

    @Autowired UnderstatRepository repo;
    @Autowired UnderstatImporter importer;
    @Autowired SaveIdDAO saveIdDAO;
    @Autowired @Qualifier("importSelenium")
    private final ThreadPoolTaskExecutor executor;
    private final ConcurrentLinkedQueue<WebDriver> driversQueue = new ConcurrentLinkedQueue<>();
    private float progress = 0;
    private int total = 0;
    private League currentLeague;
    private Integer currentYear;

    public UnderstatCrawler(@Qualifier("importSelenium")ThreadPoolTaskExecutor executor) {
        this.executor = executor;
//        driversQueue.add(new FirefoxDriver());
//        driversQueue.add(new FirefoxDriver());
//        driversQueue.add(new FirefoxDriver());
    }

    public void crawlLeagueStat(League league, int year){
        currentLeague = league;
        currentYear = year;
        startExecutor();
        importer.importLeague(getDriverOrWait(),league,year);
        List<String> teamNames = repo.getTeamsNamesFromLeague(league,year);
        total = teamNames.size();
        for(String teamName:teamNames){
            importTeamSeason(teamName,league,year);
            crawlTeamMatchesAndPlayers(teamName,league,year);
        }
        progress=0;
    }

    public void crawlTeamMatchesAndPlayers(String teamName, League league, int year){
        repo.getTeamMatchesIds(teamName,league).stream()
        .filter(matchIsNotExisted())
        .forEach(teamMatchId ->
                executor.execute(new ImportMatchThread(getDriverOrWait(),this,importer,teamMatchId)));
        progress+=0.5;
        waitUntilQueueProcess();

        repo.getMatchPlayersIds(teamName,year).stream()
        .filter(playerIsNotExisted())
        .forEach(playersId->
                executor.execute(new ImportPlayerThread(getDriverOrWait(),this,importer,playersId)));
        progress+=0.5;
    }

    private void importTeamSeason(String team, League league, Integer year){
        if (saveIdDAO.isTeamNotExisted(team,league,year)){
            importer.importTeamSeason(getDriverOrWait(),team,league,year);
            return;
        }
    }

    private Predicate<String> matchIsNotExisted(){
        return match -> saveIdDAO.isMatchNotExisted(match);
    }

    private Predicate<String> playerIsNotExisted(){
        return player -> saveIdDAO.isPlayerNotExisted(player);
    }

    private void waitUntilQueueProcess() {
        while (driversQueue.size()==0){ // all web drivers are busy
            sleep(500);
        }
    }

    private void startExecutor(){
        if (executor==null)
            executor.initialize();
    }

    public void stopExecutor() {
        executor.shutdown();
    }

    public String showProgress(){
        return "Progress "+progress+" of total "+total+". Queue size "+executor.getThreadPoolExecutor().getQueue().size()
                +" Drivers queue "+driversQueue.size()+" Current League "+currentLeague.toString()+" Current year "+currentYear;
    }

    private WebDriver getDriverOrWait(){
        while (driversQueue.isEmpty()){
            sleep(100);
        }
        return driversQueue.poll();
    }

    public void addToQueue(WebDriver webDriver) {
        driversQueue.add(webDriver);
    }

    private void sleep(long ms){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
