package ru.titus.crawler.service.radar.impl;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SportRadarPlayerClient {

    @GET("betradar/en/Europe:Berlin/gismo/match_squads/{matchid}")
    Call<String> getMatchSquads(@Path("matchid") Integer matchId);

    @GET("betradar/en/Europe:Berlin/gismo/stats_player_facts/{playerid}")
    Call<String> getPlayerStats(@Path("playerid") Integer playerid);
}
