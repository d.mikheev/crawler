package ru.titus.crawler.service.radar.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.titus.crawler.service.radar.MatchServiceInvoker;
import ru.titus.crawler.entities.ImportEntity;
import ru.titus.storage.mongo.RadarRepository;

@Component
@Scope("prototype")
public class ImportRadarThread implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(ImportRadarThread.class);
    private ImportEntity entity;
    @Autowired MatchServiceInvoker invoker;
    @Autowired RadarRepository radarRepository;

    public ImportRadarThread(){}

    public ImportRadarThread(ImportEntity importEntity){
        this.entity = importEntity;
    }

    private void parseAndSaveDocument(){
        String dublicateLink = entity.getQueryUrl() + String.valueOf(entity.getId());
        radarRepository.saveDocument(dublicateLink,entity.getCollectionName(),entity.getData());
        log.info("{} id={},succesfully saved",entity.getCollectionName(),entity.getId());
    }

    @Override
    public void run() {
        entity = invoker.enrich(entity);
        if (entity.isValid())
            parseAndSaveDocument();
    }
}
