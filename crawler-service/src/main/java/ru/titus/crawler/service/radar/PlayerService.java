package ru.titus.crawler.service.radar;

public interface PlayerService {

    public String getMatchSquads(Integer matchId);

    public String getPlayerStats(Integer playerid);

}
