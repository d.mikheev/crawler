package ru.titus.crawler.service.radar.impl;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SportRadarMatchClient {

    @GET("betradar/en/Europe:Berlin/gismo/match_detailsextended/{matchid}")
    Call<String> detailsExtended(@Path("matchid") Integer matchId);

    @GET("betradar/en/Europe:Berlin/gismo/match_info/{matchid}")
    Call<String> matchInfo(@Path("matchid") Integer matchId);

    @GET("betradar/en/Europe:Berlin/gismo/event_fullfeed/{date_offset}/{service_id}")
    Call<String> getMatchList(@Path("date_offset") long dateOffset, @Path("service_id") int serviceId);

    @GET("common/en/Europe:Berlin/gismo/stats_match_situation/{matchid}")
    Call<String> getMatchSituation(@Path("matchid") Integer matchId);

    @GET("betradar/en/Europe:Berlin/gismo/stats_team_lastx/{teamid}")
    Call<String> getTeamLastMatches(@Path("teamid") Integer teamid);

    @GET("betradar/en/Europe:Berlin/gismo/stats_match_form/{matchid}")
    Call<String> getTeamForm(@Path("matchid") Integer matchId);

    @GET("betradar/en/Europe:Berlin/gismo/stats_team_versusrecent/{homeid}/{awayid}")
    Call<String> getTeamsHeadToHeadMatches(@Path("homeid") Integer homeid, @Path("awayid") Integer awayid);

    @GET("betradar/en/Europe:Berlin/gismo/match_timeline/{matchid}")
    Call<String> getMatchTimeline(@Path("matchid") Integer matchId);

    @GET("sportradar/en/Europe:Berlin/gismo/stats_season_fixtures2/{seasonid}")
    Call<String> getSeasonPastMatches(@Path("seasonid") Integer seasonid);
}
