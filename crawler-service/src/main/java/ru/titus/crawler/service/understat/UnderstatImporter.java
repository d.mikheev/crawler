package ru.titus.crawler.service.understat;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.crawler.entities.understat.*;
import ru.titus.crawler.service.UnderstatCrawler;
import ru.titus.crawler.utils.parse.UnderstatJsonParser;
import ru.titus.storage.mongo.UnderstatRepository;
import ru.titus.storage.mongo.dto.League;
import ru.titus.storage.postgres.DAO.SaveIdDAO;

import java.util.List;

@Service
public class UnderstatImporter {

    public final UnderstatJsonParser staticParser = new UnderstatJsonParser();
    @Autowired
    UnderstatRepository repo;
    @Autowired
    SaveIdDAO saveIdDAO;
    @Autowired
    UnderstatCrawler crawler;

    public void importLeague(WebDriver driver, League league, Integer year) {
        driver.navigate().to(UnderstatImportEntity.baseUrl + "league/" + league + "/" + year);
        WebElement elem = driver.findElement(By.tagName("script"));
        String datesData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(DatesData.var), elem);
        String teamsData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(TeamsDate.var), elem);
        String playersData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(PlayersData.var), elem);
        List<String> dates = staticParser.parseJsonArrayToStrings(datesData);
        List<String> players = staticParser.parseJsonArrayToStrings(playersData);
        repo.saveDocument(new TeamsDate(teamsData, league, year));
        dates.forEach(d -> repo.saveDocument(new DatesData(d, league, year)));
        players.forEach(d -> repo.saveDocument(new PlayersData(d, staticParser.getElementValue(d, "id"), league, year)));
        crawler.addToQueue(driver);
    }

    public void importTeamSeason(WebDriver driver, String team, League league, Integer year) {
        driver.navigate().to(UnderstatImportEntity.baseUrl + "team/" + team + "/" + year);
        WebElement elem = driver.findElement(By.tagName("script"));
        String datesData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(DatesData.var), elem);
        String statisticsData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(StatisticsData.var), elem);
        String playersData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(PlayersData.var), elem);
        List<String> dates = staticParser.parseJsonArrayToStrings(datesData);
        List<String> players = staticParser.parseJsonArrayToStrings(playersData);
        saveIdDAO.saveTeamId(team, league, year);
        repo.saveDocument(new StatisticsData(statisticsData, team, year, league));
        dates.forEach(d -> repo.saveDocument(new DatesData(d, year, team, league)));
        players.forEach(d -> repo.saveDocument(new PlayersData(d, staticParser.getElementValue(d, "id"), year, team, league)));
        crawler.addToQueue(driver);
    }

    public void importMatch(WebDriver driver, String matchId) {
        driver.navigate().to(UnderstatImportEntity.baseUrl + "match/" + matchId);
        WebElement elem = driver.findElement(By.tagName("script"));
        String shotsData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(ShotsData.var), elem);
        String matchInfo = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(MatchInfo.var), elem);
        String rostersData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(RostersData.var), elem);
        saveIdDAO.saveMatchId(matchId);
        repo.saveDocument(new ShotsData(shotsData, matchId));
        repo.saveDocument(new MatchInfo(matchInfo, matchId));
        repo.saveDocument(new RostersData(rostersData, matchId));
    }

    public void importPlayer(WebDriver driver, String playerId) {
        driver.navigate().to(UnderstatImportEntity.baseUrl + "player/" + playerId);
        WebElement elem = driver.findElement(By.tagName("script"));
        String groupsData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(GroupsData.var), elem);
        String minMaxPlayerStats = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify(MinMaxPlayersStats.var), elem);
        String positionsList = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify("positionsList"), elem);
        String shotsData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify("shotsData"), elem);
        String player_info = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify("player_info"), elem);
        String matchesData = (String) ((JavascriptExecutor) driver).executeScript(staticParser.prepareJSONStringify("matchesData"), elem);

        saveIdDAO.savePlayerId(playerId);
        repo.saveDocument(new GroupsData(groupsData, playerId));
        repo.saveDocument(new MinMaxPlayersStats(minMaxPlayerStats, playerId));
        repo.saveDocument(new PlayerPositions(staticParser.placeJsonArrayInElement(positionsList, "positions"), playerId));
        repo.saveDocument(new PlayerShots(staticParser.placeJsonArrayInElement(shotsData, "playerShots"), playerId));
        repo.saveDocument(new PlayerId(player_info, playerId));
        repo.saveDocument(new PlayerMatchesData(staticParser.placeJsonArrayInElement(matchesData, "matchesData"), playerId));
    }

}
