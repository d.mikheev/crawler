package ru.titus.crawler.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import ru.titus.crawler.entities.ImportEntity;
import ru.titus.crawler.entities.radar.*;
import ru.titus.crawler.service.radar.thread.ImportRadarThread;
import ru.titus.crawler.service.radar.MatchService;
import ru.titus.crawler.service.radar.PlayerService;
import ru.titus.crawler.utils.parse.radar.JsonMatchParser;
import ru.titus.crawler.utils.parse.radar.JsonPlayerParser;
import ru.titus.storage.mongo.dto.MatchDto;
import ru.titus.storage.mongo.dto.PlayerDto;

import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class RadarCrawler {

    private static final Logger log = LoggerFactory.getLogger(RadarCrawler.class);
    private final static JsonMatchParser matchUtils = new JsonMatchParser();
    private final static JsonPlayerParser playerUtils = new JsonPlayerParser();

    @Autowired MatchService matchService;
    @Autowired PlayerService playerService;
    @Autowired private ApplicationContext applicationContext;
    @Autowired @Qualifier("importRadar")
    private final ThreadPoolTaskExecutor executor;
    private ConcurrentLinkedQueue<ImportEntity> entities = new ConcurrentLinkedQueue<>();

    public RadarCrawler(@Qualifier("importRadar")ThreadPoolTaskExecutor executor) {
        this.executor = executor;
    }

    public void addDataFromMatchPlayersToQueue(Integer matchId){
        String playersJson = playerService.getMatchSquads(matchId);
        Set<PlayerDto> players = playerUtils.parseMatchPlayers(playersJson);
        players.forEach(player -> entities.add(new Player(player.getId())));
    }

    public void addDataFromMatchToQueue(int matchid){
        entities.add(new MatchSituation(matchid));
        entities.add(new MatchInfo(matchid));
        entities.add(new MatchDetails(matchid));
        entities.add(new MatchSquads(matchid));
        entities.add(new MatchTimeline(matchid));
    }

    public void addDataFromMatchesToQueue(Set<MatchDto> matches){
        for (MatchDto match:matches){
            addDataFromMatchToQueue(match.getMatchId());
        }
    }

    public void addDataFromSeasonToQueue(Integer seasonId) {
        String matchesJson = matchService.getSeasonPastMatches(seasonId);
        entities.add(new Season(seasonId));
        Set<MatchDto> seasonMatches = matchUtils.parseSeasonMatch(matchesJson);
        addDataFromMatchesToQueue(seasonMatches);
        log.info("Succesfully added {} matches, from season {}",seasonMatches.size(), seasonId);
        log.info("Queue size={}",entities.size());
    }

    public void processQueue(){
        executor.initialize();
        while(!entities.isEmpty()){
            ImportEntity entity = entities.poll();
            ImportRadarThread importThread = (ImportRadarThread)applicationContext.getBean("importThread",entity);
            executor.execute(importThread);
        }
        executor.shutdown();
    }

}
