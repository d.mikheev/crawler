package ru.titus.crawler.service.cbr;

/**
 * Currency type for different currencies
 */
public enum Currency {
    EUR(978,"€"),
    USD(840,"$");

    private final int code;
    private final String symbol;

    Currency(int code, String symbol){
        this.code = code;
        this.symbol = symbol;
    }

    public static boolean isValid(String currencyString){
        try {
            valueOf(currencyString);
        }
        catch (IllegalArgumentException e){
            return false;
        }
        return true;
    }

    public String getSymbol() {
        return symbol;
    }
}
