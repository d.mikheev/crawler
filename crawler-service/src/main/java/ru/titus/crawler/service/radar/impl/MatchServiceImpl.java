package ru.titus.crawler.service.radar.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.crawler.service.radar.MatchService;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class MatchServiceImpl implements MatchService {

    private static final Logger logger = LoggerFactory.getLogger( MatchServiceImpl.class);
    private static final int MATCH_INFO_SERVICE_ID = 24;
    private final SportRadarMatchClient client;

    @Autowired
    public MatchServiceImpl(SportRadarMatchClient client) {
        this.client = client;
    }

    @Override
    public String getMatchDetails(Integer matchId) {
        try {
            return client.detailsExtended(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchInfo(Integer matchId) {
        try {
            return client.matchInfo(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchesTimetable(Date date) {
        try {
            //FIXME: probably the timezone issues can occur. Timezone needs to be specified explicitly
            long daysBetween = DAYS.between( LocalDate.now(), date.toInstant().atZone( ZoneId.systemDefault()).toLocalDate());
            String matchInfoJson = client.getMatchList(daysBetween, MATCH_INFO_SERVICE_ID).execute().body();
            return matchInfoJson;
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchSituation(Integer matchId) {
        try {
            return client.getMatchSituation(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getTeamLastMatches(Integer teamId) {
        try {
            return client.getTeamLastMatches(teamId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getTeamForm(Integer matchId) {
        try {
            return client.getTeamForm(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getTeamsHeadToHeadMatches(Integer homeid, Integer awayid) {
        try {
            return client.getTeamsHeadToHeadMatches(homeid,awayid).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchTimeline(Integer matchId) {
        try {
            return client.getMatchTimeline(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getSeasonPastMatches(Integer seasonid) {
        try {
            return client.getSeasonPastMatches(seasonid).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }
}
