package ru.titus.crawler.service.yandex;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.crawler.entities.understat.PlayersData;

import java.util.List;

@Service
public class WebSiteInfoFinder {

    @Autowired
    SemanticParser semanticParser;

    public String getContactPageContent(WebDriver driver, String domainUrl) {
        driver.navigate().to(domainUrl);
        WebElement siteMap = tryToFindByLink(driver,"Карта Сайта");
        WebElement contacts = tryToFindByLink(driver,"Контакты");
        WebElement requizites =  tryToFindByLink(driver,"Реквизиты компании");
        WebElement contactsXPath = tryToFindByXPath(driver, "//*[contains(text(), 'Контакты')]");
        WebElement RequizitesXPath = tryToFindByXPath(driver, "//*[contains(text(), 'Реквизиты')]");
        WebElement SiteMapXPath = tryToFindByXPath(driver, "//*[contains(text(), 'Карта')]");
//        if (RequizitesXPath!=null){
//            WebDriverWait wait1 = new WebDriverWait(driver, 10);
//            WebElement element1 = wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Реквизиты')]")));
//            element1.click();
//            return driver.getPageSource();
////            String js = "arguments[0].setAttribute('value','"+"Реквизиты"+"')";
////            return (String)((JavascriptExecutor) driver).executeScript(js, RequizitesXPath);
//        }
//        if (SiteMapXPath!=null){
//            SiteMapXPath.click();
//            return driver.getPageSource();
//        }
        if (contacts!=null){
            contacts.click();
            List<WebElement> els = driver.findElements(By.cssSelector("*"));
            return semanticParser.findAddress(els);
        }

        WebElement elem = driver.findElement(By.tagName("script"));
        return "";
    }

    private WebElement tryToFindByLink(WebDriver driver, String linkText){
        try {
            return driver.findElement(By.partialLinkText(linkText));
        }
        catch (NoSuchElementException e){
            return null;
        }
    }

    private WebElement tryToFindByXPath(WebDriver driver, String xpath){
        try {
            return driver.findElement(By.xpath(xpath));
        }
        catch (NoSuchElementException e){
            return null;
        }
    }
}
