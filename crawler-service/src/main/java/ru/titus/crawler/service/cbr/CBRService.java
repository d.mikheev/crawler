package ru.titus.crawler.service.cbr;

import java.util.Date;

public interface CBRService {

    public ValuteContainer getCurrencyRateByCurrencyAndRate(Currency currency, Date date);
}
