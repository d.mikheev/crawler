package ru.titus.crawler.service.yandex;

import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SemanticParser {

    public String findAddress(List<WebElement> els){
        for(WebElement webElement:els){
            if (webElement.getText().contains("Москва"))
                return webElement.getText();
        }
        return "";
    }


}
