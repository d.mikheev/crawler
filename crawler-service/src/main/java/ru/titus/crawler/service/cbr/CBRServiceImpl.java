package ru.titus.crawler.service.cbr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CBRServiceImpl implements CBRService {

    @Autowired
    private CBRClient cbrClient;
    private static final Logger logger = LoggerFactory.getLogger(CBRServiceImpl.class);

    @Override
    public ValuteContainer getCurrencyRateByCurrencyAndRate(Currency currency, Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dateFormat.format(date);
        try {
            String string = cbrClient.getDailyRates(strDate).execute().body();
            JAXBContext jaxbContext = JAXBContext.newInstance(ValuteContainer.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(string);
            return  (ValuteContainer) unmarshaller.unmarshal(reader);
        } catch (IOException | JAXBException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }

    }
}
